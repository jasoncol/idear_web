from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'webapp.views.home', name='home'),
     #url(r'^$', TemplateView.as_view(template_name="web_spa/index.html") , name='home'),
     url(r'^quienes-somos/$', 'webapp.views.static_content_list', name='bio'),
     url(r'^contacto/$', 'webapp.views.contact', name='contact'),
     url(r'^proyectos/$', 'webapp.views.project_list', name='projects'),
     url(r'^investigaciones/$', 'webapp.views.research_list', name='researchs'),
     url(r'^destacados/(?P<page_title>.*)/(?P<sid>\d{1,3})/$', 'webapp.views.view_hottopic', name='view_hot'),
     url(r'^destacados/$', 'webapp.views.hottopic_list', name='hot_list'),
     url(r'^servicios/(?P<page_title>.*)/(?P<sid>\d{1,3})/$', 'webapp.views.service_list', name='services'),
     url(r'^servicios/$', 'webapp.views.service_list', name='services_index'),
     url(r'^laboratorio/$', 'webapp.views.laboratory', name='laboratory'),
    # url(r'^idear_web/', include('idear_web.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
