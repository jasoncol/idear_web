$(document).ready(
    function(){
            //Por cada .mapg  en mi document asignele el innerhtml = data-custom_attr
            $('.map-canvas').each(
                function(i, obj) {
                    var lat = parseFloat($(this).attr('data-lat'));
                    var lng = parseFloat($(this).attr('data-lng'));
                    //var lat_str = parseFloat('6.555555');
                    
                    var LatLng = new google.maps.LatLng(lat,lng);
                    
                    var mapOptions = { zoom: 16, center: LatLng, mapTypeId: google.maps.MapTypeId.ROADMAP };
                    var mymap = new google.maps.Map(this, mapOptions);   
                    
                    var marker = new google.maps.Marker({ position: LatLng, map: mymap, title: $(this).attr('data-mark')});
                }                          
            );
    }    
);
