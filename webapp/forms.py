from django import forms

class ContactForm(forms.Form):
    name = forms.CharField(max_length=150, label='Nombre')
    corp = forms.CharField(max_length=150, label='Empresa', required=False)
    phone = forms.CharField(max_length=40, label='Telefono', required=False)
    email = forms.EmailField(max_length=150)
    subject = forms.CharField(max_length=40, label='Asunto', required=False)
    message = forms.CharField(max_length=400, widget=forms.widgets.Textarea(), label='Mensaje')

