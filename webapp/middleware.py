from webapp.models import Company


class WebBaseMiddleware():

    def process_template_response(self, request, response):
        pass


def company_context(request):
    try:
        return {'company' : Company.objects.get(pk=1)}
    except:
        return {'company': None}
      
