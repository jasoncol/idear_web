# Create your views here.
from django.core.mail import send_mail, EmailMessage
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.template import RequestContext
from django.http import HttpResponse
from forms import ContactForm
from models import Project, Service, Research, Clients, HomeImage, HotTopic, StaticContent, HeadingContent

def test(request):
    t = get_template('landing_page.html')
    c = RequestContext(request, {})
    return HttpResponse(t.render(c))

def get_heading(pk):
    return HeadingContent.objects.get(pk=pk)

def home(request):
    t = get_template('home.html')
    clients = Clients.objects.all()
    homeimages = HomeImage.objects.all()
    hottopics = HotTopic.objects.all().order_by('-pub_date','-id')[:4]
    services = Service.objects.filter(parent=None)[:4]
    projects = Project.objects.prefetch_related('images').all()[:5] #TODO order by
    c = RequestContext(request, {'clients': clients,'projects': projects, 'noscroll':True, 'gray_box': True, 'homeimages': homeimages, 'hottopics': hottopics,  'services': services})
    return HttpResponse(t.render(c))

def project_list(request):
    t = get_template('project_list.html')

    projects = Project.objects.prefetch_related('images').all() #TODO order by
    c = RequestContext(request, {'use_container': True, 'projects': projects, 'total_ent': len(projects), 'noscroll':True,  'heading': get_heading(3),})

    return HttpResponse(t.render(c))

def research_list(request):
    t = get_template('research_list.html')

    researchs = Research.objects.prefetch_related('images').all() #TODO order by
    c = RequestContext(request, {'researchs': researchs, 'total_ent': len(researchs), 'noscroll':True, 'heading': get_heading(4)})

    return HttpResponse(t.render(c))


def service_list(request,page_title='',  sid=''):
    t = get_template('service_list.html')
    if sid == '': sid = 59
    cats = Service.objects.filter(parent=None)
    curserv = Service.objects.get(pk=sid)
    rcats = []
    for i, c in enumerate(cats):
        c.level = 1
        c.css = i
        rcats.append(c)
        subcats = Service.objects.filter(parent=c)
        for sc in subcats:
            sc.level = 2
            sc.css = i
            rcats.append(sc)
    serv_subcats = Service.objects.filter(parent=curserv)
    c = RequestContext(request, {'use_container': True, 'cats': rcats,'curserv': curserv,'serv_subcats': serv_subcats, 'noscroll':True,  'heading': get_heading(2), 'show_menu': True if 'show' in request.GET else False})
    return HttpResponse(t.render(c))

def contact(request):
    msg = ''
    cform = ContactForm(request.POST if request.method == 'POST' else None)
    if request.method == 'POST':
        #send mail
        if cform.is_valid():
            #send mail
            #TODO spam protection
            try:
                d = cform.cleaned_data
                msg2send = "Nombre: %s <br/> Empresa: %s <br/>Telefono: %s <br/>Email: %s <br/>Asunto: %s<br/><br/>%s"%(d['name'], d['corp'], d['phone'], d['email'], d['subject'], d['message'])
                #send_mail("Contacto desde Web www.idearsas.com: " + d['subject'], msg2send, "noreply@idearsas.com", ["test@idearsas.com"], False)
                em = EmailMessage("Contacto desde Web www.idearsas.com: " + d['subject'], msg2send, "noreply@idearsas.com", ["info@idearsas.com"], headers={'Reply-to': d['email']})
                em.content_subtype = 'html'
                em.send()
                
                msg = 'Mensaje enviado exitosamente. Muchas gracias.' 
            except:
                msg = 'Ha ocurrido un error enviando el mensaje. Por favor intente de nuevo.'
        else:
            msg = 'Ha ocurrido un error enviando el mensaje'
        pass
    t = get_template('contact.html')
    c = RequestContext(request, {'use_container': True, 'msg': msg, 'cform': cform, 'noscroll':True})

    return HttpResponse(t.render(c))
def view_hottopic(request, page_title, sid):
    t = get_template('hot_view.html')
    hottopic = HotTopic.objects.get(pk=sid)
    c = RequestContext(request, {'use_container': True, 'hottopic': hottopic, 'noscroll':True})
    return HttpResponse(t.render(c))

def hottopic_list(request):
    t = get_template('hot_list.html')
    hottopics = HotTopic.objects.all().order_by('-pub_date','-id') #TODO apginator
    c = RequestContext(request, {'use_container': True, 'hottopics': hottopics, 'noscroll':True})
    return HttpResponse(t.render(c))

def laboratory(request):
    t = get_template('laboratory.html')

    c = RequestContext(request, {'use_container': True, 'noscroll':True,  'heading': get_heading(5)})
    return HttpResponse(t.render(c))
	
def static_content_list(request,page_title='',  sid=''):
    t = get_template('bio.html')
    contents = StaticContent.objects.all().order_by('order')
    c = RequestContext(request, {'use_container': True, 'contents': contents, 'noscroll':True, 'heading': get_heading(1)})
    return HttpResponse(t.render(c))
