# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Clients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('content', models.TextField(max_length=2000, null=True, verbose_name=b'Contenido', blank=True)),
                ('pub_date', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Nombre')),
                ('short_name', models.CharField(max_length=200, verbose_name=b'Nombre corto')),
                ('address', models.CharField(max_length=200, verbose_name=b'Direcci\xc3\xb3n')),
                ('phone', models.CharField(max_length=200, verbose_name=b'Tel\xc3\xa9fono')),
                ('email', models.EmailField(max_length=75, verbose_name=b'Email')),
                ('map_lat', models.CharField(max_length=30, verbose_name=b'Mapa - Latitud')),
                ('map_lng', models.CharField(max_length=30, verbose_name=b'Mapa - Longitud')),
                ('slogan', models.TextField(max_length=1000, verbose_name=b'Slogan largo')),
                ('short_slogan', models.TextField(default=b'', max_length=300, verbose_name=b'Slogan corto', blank=True)),
            ],
            options={
                'verbose_name': 'Compa\xf1\xeda',
                'verbose_name_plural': 'Compa\xf1\xeda',
            },
            bases=(models.Model,),
        ),
          migrations.CreateModel(
            name='HomeImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('desc', models.CharField(max_length=200, verbose_name=b'Descripci\xc3\xb3n corta')),
                ('picture', models.ImageField(upload_to=b'img_portada', verbose_name=b'Imagen')),
                ('order', models.PositiveIntegerField(default=10, verbose_name=b'Orden de aparici\xc3\xb3n')),
                ('link', models.CharField(max_length=200, null=True, verbose_name=b'Link', blank=True)),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Imagen en Portada',
                'verbose_name_plural': 'Im\xe1genes de Portada',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HotTopic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('pub_date', models.DateField(auto_now=True)),
                ('content', ckeditor.fields.RichTextField(verbose_name=b'Contenido')),
            ],
            options={
                'ordering': ('-pub_date', '-id'),
                'verbose_name': 'Destacado',
                'verbose_name_plural': 'Destacados',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('desc', models.CharField(max_length=200, verbose_name=b'Descripci\xc3\xb3n corta')),
                ('picture', models.ImageField(upload_to=b'img', verbose_name=b'Imagen')),
                ('order', models.PositiveIntegerField(default=10, verbose_name=b'\xc3\x93rden de aparici\xc3\xb3n')),
                ('instance_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Imagen',
                'verbose_name_plural': 'Im\xe1genes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('content', models.TextField(max_length=2000, null=True, verbose_name=b'Contenido', blank=True)),
                ('pub_date', models.DateField(auto_now=True)),
                ('begin_date', models.DateField(verbose_name=b'Fecha inicio')),
                ('end_date', models.DateField(null=True, verbose_name=b'Fecha fin', blank=True)),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Research',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('content', models.TextField(max_length=2000, null=True, verbose_name=b'Contenido', blank=True)),
                ('pub_date', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Investigaci\xf3n',
                'verbose_name_plural': 'Investigaciones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('content', models.TextField(max_length=2000, null=True, verbose_name=b'Contenido', blank=True)),
                ('pub_date', models.DateField(auto_now=True)),
                ('parent', models.ForeignKey(blank=True, to='webapp.Service', null=True)),
            ],
            options={
                'verbose_name': 'Servicio',
                'verbose_name_plural': 'Servicios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StaticContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('content', ckeditor.fields.RichTextField(verbose_name=b'Contenido')),
                ('order', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Contenido Quienes somos',
                'verbose_name_plural': 'Contenidos Quienes somos',
            },
            bases=(models.Model,),
        ),
    ]
