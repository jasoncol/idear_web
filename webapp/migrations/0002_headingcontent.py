# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HeadingContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, verbose_name=b'T\xc3\xadtulo')),
                ('content', ckeditor.fields.RichTextField(verbose_name=b'Contenido')),
            ],
            options={
                'verbose_name': 'Contenido a',
                'verbose_name_plural': 'Contenidos a',
            },
            bases=(models.Model,),
        ),
    ]
