# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0002_headingcontent'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='company',
            options={'verbose_name': 'Informaci\xf3n empresa', 'verbose_name_plural': 'Informaci\xf3n empresa'},
        ),
        migrations.AlterModelOptions(
            name='headingcontent',
            options={'verbose_name': 'Cabecera', 'verbose_name_plural': 'Cabeceras'},
        ),
    ]
