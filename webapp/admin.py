from django.contrib import admin
from models import Project, Service, Research, Clients, Image, HomeImage, HotTopic, StaticContent, Company, HeadingContent
from django.contrib.sites.models import Site 
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.admin import GenericTabularInline

class ImageInline(GenericTabularInline):
    ct_fk_field = 'instance_id'
    model = Image

class ProjectAdmin(admin.ModelAdmin):
    inlines = [ImageInline]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.resize = True
            instance.save()
        formset.save_m2m()

class ServiceAdmin(admin.ModelAdmin):
    inlines = [ImageInline]
    
    def parent(self, obj):
        if obj.parent is not None:
            return '%s'%(obj.parent)

    list_display=('id', 'title', 'parent')
    list_display_links=('title',)

class ResearchAdmin(admin.ModelAdmin):
    inlines = [ImageInline]

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.resize = True
            instance.save()
        formset.save_m2m()

class ClientAdmin(admin.ModelAdmin):
    inlines = [ImageInline]

class CompanyAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Research, ResearchAdmin)
admin.site.register(Clients, ClientAdmin)
admin.site.register(HomeImage)
admin.site.register(HotTopic)
admin.site.register(StaticContent)
admin.site.register(HeadingContent)
admin.site.register(Company, CompanyAdmin)
