# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from ckeditor.fields import RichTextField
from django.utils.html import strip_tags
from PIL import Image as PILImage
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
# Create your models here.

class GenericModel(models.Model):
    title = models.CharField(max_length=200, verbose_name='Título')
    content = models.TextField(max_length=2000, verbose_name='Contenido', null=True, blank=True)
    pub_date = models.DateField(auto_now=True)

    def __unicode__(self):
        return "%s - %s"%(self.pk, self.title)
    
    def safe_title(self):
        return self.title.replace(' ', '_')

    class Meta:
        abstract = True

class Company(models.Model):
    name = models.CharField('Nombre', max_length=200)
    short_name = models.CharField('Nombre corto', max_length=200)
    address = models.CharField('Dirección', max_length=200)
    phone = models.CharField('Teléfono', max_length=200)
    email = models.EmailField('Email')
    map_lat = models.CharField('Mapa - Latitud', max_length=30)
    map_lng = models.CharField('Mapa - Longitud', max_length=30)
    slogan = models.TextField('Slogan largo', max_length=1000)
    short_slogan = models.TextField('Slogan corto',  max_length=300, default='', blank=True)
    prices_pdf = models.FileField('PDF listado de precios', null=True, blank=True)

    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Información empresa'
        verbose_name_plural = 'Información empresa'

class Project(GenericModel):
    begin_date = models.DateField(verbose_name='Fecha inicio')
    end_date = models.DateField(verbose_name='Fecha fin', null=True, blank=True)
    images = GenericRelation('Image',
                               content_type_field='content_type',
                               object_id_field='instance_id')
    class Meta:
        verbose_name='Proyecto'
        verbose_name_plural='Proyectos'
        ordering=['-id']

class Service(GenericModel):
    parent = models.ForeignKey('Service', null=True, blank=True)
    images = GenericRelation('Image',
                               content_type_field='content_type',
                               object_id_field='instance_id')
 
    class Meta:
        verbose_name='Servicio'
        verbose_name_plural='Servicios'


class Research(GenericModel):
    images = GenericRelation('Image',
                               content_type_field='content_type',
                               object_id_field='instance_id')
 
    class Meta:
        verbose_name='Investigación'
        verbose_name_plural='Investigaciones'

class Clients(GenericModel):
    images = GenericRelation('Image',
                               content_type_field='content_type',
                               object_id_field='instance_id')
 
    class Meta:
        verbose_name='Cliente'
        verbose_name_plural='Clientes'



class Image(models.Model):
    ''' Model to store pictures in the DB. Pictures are associated to several model types (content type abstraction) '''

    title = models.CharField(max_length=200, verbose_name='Título')
    desc = models.CharField(max_length=200, verbose_name='Descripción corta')
    picture = models.ImageField(upload_to='img', verbose_name='Imagen')
    order = models.PositiveIntegerField(default=10, verbose_name='Órden de aparición')
    content_type = models.ForeignKey(ContentType)
    instance_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'instance_id')
    resize = False

    def __unicode__(self):
        return self.title
	
    def save(self, *args, **kwargs):
        super(Image, self).save(*args, **kwargs)
        if self.resize:
            # If Image size > 640x480, resize it
            img = None
            try:
                img = PILImage.open(self.picture.path)
            except:
                pass
            if img is None:
                return
            w, h = img.size
		
            lw = 640
            lh = 480
            if(w > lw):
                x_rat = float(lw)/w
                w = w*x_rat
                h = h*x_rat
            if(h > lh and h != 0):
                y_rat = float(lh)/h
                w = w*y_rat
                h = h*y_rat

            img = img.resize((int(w), int(h)), PILImage.ANTIALIAS)
            img.save(self.picture.path, 'JPEG', quality=90)
        
    class Meta:
        verbose_name='Imagen'
        verbose_name_plural='Imágenes'
	

class HomeImage(models.Model):
    title = models.CharField(max_length=200, verbose_name='Título')
    desc = models.CharField(max_length=200, verbose_name='Descripción corta')
    picture = models.ImageField(upload_to='img_portada', verbose_name='Imagen')
    order = models.PositiveIntegerField(default=10, verbose_name='Orden de aparición')
    link = models.CharField(max_length=200, verbose_name='Link', blank=True, null=True)

    def __unicode__(self):
        return self.title
    
    class Meta:
        verbose_name='Imagen en Portada'
        verbose_name_plural='Imágenes de Portada'
        ordering=('order',)

class HotTopic(models.Model):
    title = models.CharField(max_length=200, verbose_name='Título')
    pub_date = models.DateField(auto_now=True)
    content = RichTextField(verbose_name='Contenido')    
    #icon = models.ImageField(upload_to='img_destacado', verbose_name='Ícono', blank=True, null=True)
    
    def __unicode__(self):
        return self.title
    def prev(self):
        max_=200
        content = strip_tags(self.content)
        return content[0:min(len(content)-1,max_)]
    class Meta:
        verbose_name = 'Destacado'
        verbose_name_plural = 'Destacados'
        ordering = ('-pub_date','-id')

class StaticContent(models.Model):
    title = models.CharField(max_length=200, verbose_name='Título')
    content = RichTextField(verbose_name='Contenido')
    order = models.IntegerField()
    
    def __unicode__(self):
        return self.title
    def prev(self):
        max_=200
        content = strip_tags(self.content)
        return content[0:min(len(content)-1,max_)]
    class Meta:
        verbose_name = 'Contenido Quienes somos'
        verbose_name_plural = 'Contenidos Quienes somos'

class HeadingContent(models.Model):
    title = models.CharField(max_length=200, verbose_name='Título')
    content = RichTextField(verbose_name='Contenido')
    
    def __unicode__(self):
        return self.title

    def prev(self):
        max_=200
        content = strip_tags(self.content)
        return content[0:min(len(content)-1,max_)]

    class Meta:
        verbose_name = 'Cabecera'
        verbose_name_plural = 'Cabeceras'
